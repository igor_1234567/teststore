from django.contrib import admin
from django.conf import settings

from .models import User, Product, Cart, CartEntry

# Register your models here.
admin.site.register(User)
admin.site.register(Product)
admin.site.register(Cart)
admin.site.register(CartEntry)

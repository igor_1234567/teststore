from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings


class User(AbstractUser):
    """ Роли пользователей: клиент, менеджер
    """
    pass


class Product(models.Model):
    """ Товары: артикул (текст), наименование, цена закуп, цена розница.
    """
    article = models.CharField(max_length=8, unique=True)
    description = models.CharField(max_length=255)
    price_net = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    price_sell = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)


class Cart(models.Model):
    """ Корзина: клиент, товар, количество, цена, сумма по строке.
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                             related_name="basket")
    items = models.ManyToManyField("Product", related_name="in_carts")


class CartEntry(models.Model):
    product = models.ForeignKey(Product, null=True, on_delete=models.CASCADE)
    cart = models.ForeignKey(Cart, null=True, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField()

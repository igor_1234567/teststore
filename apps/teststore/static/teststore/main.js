document.addEventListener('DOMContentLoaded', function() {

    // when a file selected for upload substitute a
    // label's inner html for filename
    document.querySelectorAll('input[type="file"]').forEach(upload => {
        upload.onchange = () => {
            upload.previousElementSibling.innerHTML = upload.files[0].name
        };
    });

})

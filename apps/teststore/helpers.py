import csv
from decimal import *
from io import StringIO


def read_csvfile(csv_file):
    """
    - Артикул = "ПТ"+"КодТовараПоставщика"
    - Наименование товара = "НаименованиеТовараПоставщика"
    - Цена закуп = 0.9 * "ЦенаПоставщика"
    - Цена розница = если "ЦенаПоставщика" < 1000 руб, то 1.2 * "ЦенаПоставщика", иначе 1.1 * "ЦенаПоставщика"
    """
    csv_file.seek(0)
    reader = csv.reader(StringIO(csv_file.read().decode('utf-8')),
                        delimiter=';')
    data = list(reader)

    processed_data = list()
    for row in data[1:]:
        price_net = Decimal(float(row[2])) * Decimal(0.9)
        if Decimal(row[2]) < 1000:
            price_sell = Decimal(float(row[2])) * Decimal(1.2)
        else:
            price_sell = Decimal(float(row[2])) * Decimal(1.1)
        processed_data.append(dict(article=row[0],
                                   description=row[1],
                                   price_net=price_net,
                                   price_sell=price_sell))

    return processed_data



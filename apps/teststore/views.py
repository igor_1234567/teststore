from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.db import IntegrityError
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from .models import User, Product, Cart, CartEntry
from .helpers import read_csvfile


def index_view(request):
    products = Product.objects.all()
    return render(request, 'teststore/index.html', {
        "products": products,
    })


@login_required
def cart_view(request):
    pass


@login_required
def manager_view(request):
    pass


@login_required
def import_view(request):
    """
    Интеграция с внешним поставщиком
    Скрипт, который загружает файл с товарами поставщика в наш интернет-магазин.
    """
    if request.method == 'POST':
        infile = request.FILES['file']
        data = read_csvfile(infile)
        products = Product.objects.filter(Q(article__in=[row['article'] for row in data]))
        if products:
            return render(request, 'teststore/upload.html', {
                "message": "These codes are already in use! Please try again!"
                })
        else:
            for row in data:
                Product.objects.create(article=row['article'],
                                        description=row['description'],
                                        price_net=row['price_net'],
                                        price_sell=row['price_sell'])
            return HttpResponseRedirect(reverse("index"))

    return render(request, 'teststore/upload.html', {})


def login_view(request):
    if request.method == "POST":

        # Attempt to sign user in
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)

        # Check if authentication successful
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(reverse("index"))
        else:
            return render(request, "teststore/login.html", {
                "message": "Invalid username and/or password."
            })
    else:
        return render(request, "teststore/login.html")


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse("index"))


def register(request):
    if request.method == "POST":
        username = request.POST["username"]
        email = request.POST.get("email", "email@exmaple.com")

        # Ensure password matches confirmation
        password = request.POST["password"]
        confirmation = request.POST["confirmation"]
        if password != confirmation:
            return render(request, "teststore/register.html", {
                "message": "Passwords must match."
            })

        # Attempt to create new user
        try:
            user = User.objects.create_user(username, email, password)
            user.save()
        except IntegrityError:
            return render(request, "teststore/register.html", {
                "message": "Username already taken."
            })
        login(request, user)
        return HttpResponseRedirect(reverse("index"))
    else:
        return render(request, "teststore/register.html")

from django.urls import path


from . import views

urlpatterns = [
    path("", views.index_view, name="index"),
    path("import", views.import_view, name="import"),
    path("login", views.login_view, name="login"),
    path("logout", views.logout_view, name="logout"),
    path("register", views.register, name="register"),
]
